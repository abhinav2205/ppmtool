FROM openjdk:8u131-jdk-alpine

MAINTAINER "Abhinav Sharma"

EXPOSE 8080

WORKDIR /usr/local/bin

COPY ./target/ppmtool-0.0.1-SNAPSHOT.jar webapi.jar

CMD ["java","-jar","webapi.jar"]



